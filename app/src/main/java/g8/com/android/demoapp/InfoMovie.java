package g8.com.android.demoapp;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import g8.com.android.demoapp.connectapi.NotifyDataListener;
import g8.com.android.demoapp.libs.CirclePageIndicator;
import g8.com.android.demoapp.libs.WrapContentViewPager;

/**
 * Created by Administrator on 1/5/2017.
 */

public class InfoMovie extends ParentActivity {
    private ArrayList<Model> listData;
    private WrapContentViewPager mPager;
    private CirclePageIndicator mIndicator;
    private Model md;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.info_book);
        bindActivity();
//        loadData();
        loadSimilar();
//        setData();
    }

    private void setData() {
        md = App.getFilmDetail();
        Utilities.showALog(md.getImage());
        Utilities.loadPicasso(this, md.getImage(), ((ImageView) findViewById(R.id.img)));
        Utilities.loadPicasso(this, md.getImgBG(), ((ImageView) findViewById(R.id.imgBg)));
        ((TextView) findViewById(R.id.tvDes)).setText(md.getDescription());
        ((TextView) findViewById(R.id.tvName)).setText(Utilities.getDataBundle2(this));


//        (findViewById(R.id.btnDoc)).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                Utilities.startActivity(InfoMovie.this, ListChap.class, md.getId() + "", md.getName());
//            }
//        });

    }

    public void clickExpand(View v) {
        ((TextView) findViewById(R.id.tvDes)).performClick();
    }


    private void bindActivity() {
        if (null == listData)
            listData = new ArrayList<>();
        mPager = (WrapContentViewPager) findViewById(R.id.pager);
        mIndicator = (CirclePageIndicator) findViewById(R.id.indicator);
        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar1);
        setSupportActionBar(toolbar);
//        getSupportActionBar().setTitle(Utilities.getDataBundle2(this));
        Utilities.setHeight((ImageView) findViewById(R.id.img), 2.3, this);
        showBack(true);
//        getChap(435435435);
    }

    private void showBack(boolean isShow) {
        getSupportActionBar().setDisplayHomeAsUpEnabled(isShow);
        getSupportActionBar().setDisplayShowHomeEnabled(isShow);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_info, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.fav) {
            if (!App.getDB().checkProductExists(md.getId())) {
                if (App.getDB().addNewFav(md) != -1) {
                    Toast.makeText(this, "Added to favorite", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(this, "This movie already exists in database", Toast.LENGTH_SHORT).show();
            }
            return true;
        }
        if (id == android.R.id.home) {
            this.finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void loadSimilar() {
        String url = "https://api.themoviedb.org/3/movie/" + Utilities.getDataBundle(this) + "/similar?api_key=" + ConstantValue.PRIVATE_KEY + "&language=en-US&page=1";
        HashMap<String, String> builder = getDefauldParams(url);
        callServer(this, builder, true, false, new NotifyDataListener() {
            @Override
            public void onSuccess(JSONObject result) {
                JSONArray array = null;
                try {
                    array = result.getJSONArray("results");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject ob = array.getJSONObject(i);
                        Model md = new Model();
                        md.setImage(ConstantValue.URL_IMAGE + Utilities.getDataString(ob, "poster_path"));
                        md.setDescription(Utilities.getDataString(ob, "overview"));
                        md.setName(Utilities.getDataString(ob, "title"));
                        md.setSubName(Utilities.getDataString(ob, "overview"));
                        md.setId(Utilities.getDataInt(ob, "id"));
                        md.setImgBG(ConstantValue.URL_IMAGE + Utilities.getDataString(ob, "backdrop_path"));
                        listData.add(md);
                    }
                } catch (JSONException e) {
                }
                int s = listData.size() / 2;
                if (listData.size() % 2 != 0) {
                    s++;
                }
                mPager.setAdapter(new SlidePageNews(InfoMovie.this, s, listData));
                mIndicator.setViewPager(mPager);
                setData();
            }

            @Override
            public void onFailure(int code, String message) {
            }
        });
    }
}
