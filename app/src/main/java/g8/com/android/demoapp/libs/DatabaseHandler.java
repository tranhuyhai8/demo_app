package g8.com.android.demoapp.libs;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

import g8.com.android.demoapp.Model;

public class DatabaseHandler extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "demo_app";
    private static final String TABLE_MOVIE = "movies";
    private static final String ID = "ID";
    private static final String ID_MOVIE = "mID";
    private static final String NAME = "name";
    private static final String DES = "des";
    private static final String IMAGE = "img";
    private static final String IMAGE_BG = "img_bg";
    private static final String SUB_NAME = "subname";

    SQLiteDatabase db;
    private Context c;

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        c = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_BOOK_TABLE = "CREATE TABLE " + TABLE_MOVIE + "(" + ID + " INTEGER PRIMARY KEY," + NAME + " TEXT," + ID_MOVIE + " INTEGER," + DES + " TEXT," + IMAGE + " TEXT," + IMAGE_BG + " TEXT," + SUB_NAME + " TEXT" + ")";
        db.execSQL(CREATE_BOOK_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MOVIE);
        onCreate(db);
    }

    private SQLiteDatabase getDatabase() {
        if (null == db || !db.isOpen()) {
            return db = this.getWritableDatabase();
        }
        return db;

    }

    public long addNewFav(Model md) {
        ContentValues values = new ContentValues();
        values.put(ID_MOVIE, md.getId());
        values.put(NAME, md.getName());
        values.put(DES, md.getDescription());
        values.put(IMAGE, md.getImage());
        values.put(IMAGE_BG, md.getImgBG());
        values.put(SUB_NAME, md.getSubName());
        long insertNumber = getDatabase().insert(TABLE_MOVIE, null, values);
        getDatabase().close();
        return insertNumber;
    }

    public boolean checkProductExists(int producID) {
        String selectQuery = "SELECT * FROM " + TABLE_MOVIE + " WHERE " + ID_MOVIE + "=" + producID;
        Cursor cursor = getDatabase().rawQuery(selectQuery, null);
        try {
            if (cursor.getCount() > 0) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }

        return false;
    }

    public ArrayList<Model> getListFav() {
        ArrayList<Model> list = new ArrayList<Model>();
        String selectQuery = "SELECT * FROM " + TABLE_MOVIE + " ORDER BY " + ID + " DESC";
        Cursor cursor = getDatabase().rawQuery(selectQuery, null);
        try {
            if (cursor.moveToFirst()) {
                do {
                    try {
                        Model md = new Model();
                        md.setId(cursor.getInt(cursor.getColumnIndex(ID_MOVIE)));
                        md.setName(cursor.getString(cursor.getColumnIndex(NAME)));
                        md.setDescription(cursor.getString(cursor.getColumnIndex(DES)));
                        md.setImage(cursor.getString(cursor.getColumnIndex(IMAGE)));
                        md.setImgBG(cursor.getString(cursor.getColumnIndex(IMAGE_BG)));
                        md.setSubName(cursor.getString(cursor.getColumnIndex(SUB_NAME)));
                        list.add(md);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
            getDatabase().close();
        }

        return list;
    }
}