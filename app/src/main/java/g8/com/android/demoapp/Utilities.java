package g8.com.android.demoapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.content.pm.Signature;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.provider.MediaStore;
import android.provider.Settings.Secure;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Currency;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class Utilities {

    public static String FORMAT_DATE = "dd/MM/yyyy";


    public static int getVertionCode(Context ac) {
        PackageInfo pInfo;
        int version = 0;
        try {
            pInfo = ac.getPackageManager().getPackageInfo(ac.getPackageName(), 0);
            version = pInfo.versionCode;
        } catch (NameNotFoundException e) {
            e.printStackTrace();
        }
        return version;
    }

    public static Typeface getFont(int i, Activity ac) {
        Typeface type = null;
        if (i == 0) {
            type = Typeface.createFromAsset(ac.getAssets(), "fonts/verdana.ttf");
        } else if (i == 1) {
            type = Typeface.createFromAsset(ac.getAssets(), "fonts/georgia.ttf");
        } else if (i == 2) {
            type = Typeface.createFromAsset(ac.getAssets(), "fonts/myriadpro.otf");
        } else if (i == 3) {
            type = null;
        }
        return type;
    }


    public static void sendEmail(Activity ac) {
        try {
            Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                    "mailto", "tranhuyhai8@gmail.com", null));
//        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject");
//        emailIntent.putExtra(Intent.EXTRA_TEXT, "Body");
            ac.startActivity(Intent.createChooser(emailIntent, "Yêu Cầu & Góp Ý..."));
        } catch (Exception e) {

        }

    }


    public static void setDynamicWidth(GridView gridView) {
        ListAdapter gridViewAdapter = gridView.getAdapter();
        if (gridViewAdapter == null) {
            return;
        }
        int totalWidth;
        int items = gridViewAdapter.getCount() / 2;
        View listItem = gridViewAdapter.getView(0, null, gridView);
        listItem.measure(0, 0);
        totalWidth = listItem.getMeasuredWidth();
        totalWidth = totalWidth * items;
        LayoutParams params = gridView.getLayoutParams();
        params.width = totalWidth;
        gridView.setLayoutParams(params);
    }

    public static void onShareClick(Context c, String tex) {
        List<Intent> targetShareIntents = new ArrayList<Intent>();
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        List<ResolveInfo> resInfos = c.getPackageManager().queryIntentActivities(shareIntent, 0);
        if (!resInfos.isEmpty()) {
            System.out.println("Have package");
            for (ResolveInfo resInfo : resInfos) {
                String packageName = resInfo.activityInfo.packageName;
                Log.i("Package Name", packageName);
                if (packageName.contains("com.facebook.orca") || packageName.contains("vnd.android-dir/mms-sms") || packageName.contains("mms") || packageName.contains("message")) {
                    Intent intent = new Intent();
                    intent.setComponent(new ComponentName(packageName, resInfo.activityInfo.name));
                    intent.setAction(Intent.ACTION_SEND);
                    intent.setType("text/plain");
                    intent.putExtra(Intent.EXTRA_TEXT, tex);
                    intent.putExtra(Intent.EXTRA_SUBJECT, "");
                    intent.setPackage(packageName);
                    targetShareIntents.add(intent);
                }
            }
            if (!targetShareIntents.isEmpty()) {
                System.out.println("Have Intent");
                Intent chooserIntent = Intent.createChooser(targetShareIntents.remove(0), "Choose app to share");
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetShareIntents.toArray(new Parcelable[]{}));
                c.startActivity(chooserIntent);
            } else {
                System.out.println("Do not Have Intent");
                // showDialaog(this);
            }
        }
    }


    public static double round(double value, int places) {
        try {
            if (places < 0)
                throw new IllegalArgumentException();

            BigDecimal bd = new BigDecimal(value);
            bd = bd.setScale(places, RoundingMode.HALF_UP);
            return bd.doubleValue();
        } catch (Exception e) {
            return 0;
        }

    }


    public static boolean isGoogleMapsInstalled(Activity ac) {
        try {
            ApplicationInfo info = ac.getPackageManager().getApplicationInfo("com.google.android.apps.maps", 0);
            return true;
        } catch (NameNotFoundException e) {
            return false;
        }
    }


    public static Date getDateFromString(String dateString) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(FORMAT_DATE);
        Date convertedDate = new Date();
        try {
            convertedDate = dateFormat.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return convertedDate;
    }

    public static void showKB(Activity ac, View v) {
        try {
            InputMethodManager imm = (InputMethodManager) ac.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // ac.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
    }


    public static void openBrowzer(Context ac, String urrl) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(urrl));
        ac.startActivity(browserIntent);
    }

    public static String getFacebookImage(JSONObject ob) {
        JSONObject data = null;
        String url = "";
        try {
            data = ob.getJSONObject("data");
            url = data.getString("url");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return url;

    }


    public static void hideKb(Activity ac, View v) {
        try {
            InputMethodManager imm = (InputMethodManager) ac.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static AlertDialog showDialogConfirm(Activity ac, final String message, final NotifyDataAbstract notify, String okbutton, String huy) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ac);
        builder.setTitle(ac.getString(R.string.app_name));
        builder.setIcon(R.mipmap.ic_launcher);
        builder.setMessage(message);
        builder.setPositiveButton(okbutton, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(final DialogInterface dialog, final int id) {
                if (notify != null) {
                    notify.onReturnInt(NotifyDataAbstract.NOTIFY_OK);
                }
                dialog.dismiss();
            }
        });

        builder.setNegativeButton(huy, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(final DialogInterface dialog, final int which) {
                if (notify != null) {
                    notify.onReturnInt(NotifyDataAbstract.NOTIFY_FAILED);
                }
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        return alert;
    }


    public static String getQuery(HashMap<String, String> parameters) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        // result.append("?token=" + this.token);
        for (String key : parameters.keySet()) {

            if (first) {
                first = false;
                result.append("?");
            }

            result.append("&");

            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(parameters.get(key), "UTF-8"));
        }
        showLog(result.toString());
        return result.toString();
    }


    @SuppressWarnings("deprecation")
    public static void setWH(Activity ac) {
        try {
            Display display = ac.getWindowManager().getDefaultDisplay();
            int width = display.getWidth();
            int height = display.getHeight();
            App.setDevice_height(height);
            App.setDevice_width(width);
            showALog("width = " + width + "|height = " + height);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private static boolean checkWHValue() {
        if (App.getDevice_width() == 0 || App.getDevice_height() == 0) {
            return false;
        }
        return true;
    }


    public static void setHeight(View paramView, double paramDouble, Activity activity) {
        try {
            if (!checkWHValue()) {
                setWH(activity);
            }
            LayoutParams localLayoutParams = paramView.getLayoutParams();
            localLayoutParams.height = (int) (App.getDevice_height() / paramDouble);
            paramView.setLayoutParams(localLayoutParams);
        } catch (Exception e) {
            // TODO: handle exception
        }

    }

    public static void setHeightHardCode(View paramView, double paramDouble, Activity activity) {
        try {
            if (!checkWHValue()) {
                setWH(activity);
            }
            LayoutParams localLayoutParams = paramView.getLayoutParams();
            localLayoutParams.height = (int) paramDouble;
            paramView.setLayoutParams(localLayoutParams);
            paramView.requestLayout();
        } catch (Exception e) {
            // TODO: handle exception
        }

    }

    public static void setWidth(View paramView, double paramDouble, Activity activity) {
        if (!checkWHValue()) {
            setWH(activity);
        }
        LayoutParams localLayoutParams = paramView.getLayoutParams();
        localLayoutParams.width = (int) (App.getDevice_width() / paramDouble);
        paramView.setLayoutParams(localLayoutParams);
    }

    public static void setWidthHardCode(View paramView, double paramDouble, Activity activity) {
        if (!checkWHValue()) {
            setWH(activity);
        }
        LayoutParams localLayoutParams = paramView.getLayoutParams();
        localLayoutParams.width = (int) paramDouble;
        paramView.setLayoutParams(localLayoutParams);
    }

    public static void setWidthHeight(View paramView, double paramDouble1, double paramDouble2, Activity activity) {
        if (!checkWHValue()) {
            setWH(activity);
        }
        LayoutParams localLayoutParams = paramView.getLayoutParams();
        localLayoutParams.width = (int) (App.getDevice_width() / paramDouble1);
        localLayoutParams.height = (int) (App.getDevice_height() / paramDouble2);
        paramView.setLayoutParams(localLayoutParams);
    }

    public static void setWidthHeightHardCode(View paramView, double paramDouble1, double paramDouble2, Activity activity) {
        if (!checkWHValue()) {
            setWH(activity);
        }
        LayoutParams localLayoutParams = paramView.getLayoutParams();
        localLayoutParams.width = (int) paramDouble1;
        localLayoutParams.height = (int) paramDouble2;
        paramView.setLayoutParams(localLayoutParams);
    }

    public static Bitmap drawTextToBitmap(Context mContext, int hinh, String mText) {
        try {
            Resources resources = mContext.getResources();
            float scale = resources.getDisplayMetrics().density;
            Bitmap bitmap = BitmapFactory.decodeResource(resources, hinh);
            Bitmap.Config bitmapConfig = bitmap.getConfig();
            // set default bitmap config if none
            if (bitmapConfig == null) {
                bitmapConfig = Bitmap.Config.ARGB_8888;
            }
            // resource bitmaps are imutable,
            // so we need to convert it to mutable one
            bitmap = bitmap.copy(bitmapConfig, true);
            Canvas canvas = new Canvas(bitmap);
            // new antialised Paint
            Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
            // text color - #3D3D3D
            paint.setColor(Color.WHITE);
            // text size in pixels
            paint.setTextSize((int) (12 * scale));
            // text shadow
            paint.setShadowLayer(1f, 0f, 1f, Color.WHITE);
            // draw text to the Canvas center
            Rect bounds = new Rect();
            paint.getTextBounds(mText, 0, mText.length(), bounds);
            int x = (bitmap.getWidth() - bounds.width()) / 6;
            int y = (bitmap.getHeight() + bounds.height()) / 5;
            canvas.drawText(mText, x * scale, y * scale, paint);
            return bitmap;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    public static int setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return 0;

        int desiredWidth = MeasureSpec.makeMeasureSpec(listView.getWidth(), MeasureSpec.AT_MOST);
        int totalHeight = 0;
        View view = null;
        // if (lvH == 0) {
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new LayoutParams(desiredWidth, LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
            // lvH = view.getMeasuredHeight();
            // break;
        }
        // } else {
        // totalHeight = lvH * listAdapter.getCount();
        // }
        // totalHeight = lvH * listAdapter.getCount();

        LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
        // sendMessage(HotdealApp.context, "SETH", String.valueOf(totalHeight +
        // (listView.getDividerHeight() * (listAdapter.getCount() - 1))));
        return totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
    }

    public static int setListViewHeightNomal(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return 0;

        int desiredWidth = MeasureSpec.makeMeasureSpec(listView.getWidth(), MeasureSpec.AT_MOST);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new LayoutParams(desiredWidth, LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }

        LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
        // sendMessage(HotdealApp.context, "SETH", String.valueOf(totalHeight +
        // (listView.getDividerHeight() * (listAdapter.getCount() - 1))));
        return totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
    }


    public static void setButtonFont(Context c, String namefont, Button button) {
        try {
            Typeface font = Typeface.createFromAsset(c.getAssets(), namefont);
            button.setTypeface(font);
        } catch (Exception e) {

        }

    }

    public static void setListViewHeight(ExpandableListView listView) {
        ExpandableListAdapter listAdapter = (ExpandableListAdapter) listView.getExpandableListAdapter();
        int totalHeight = 0;
        int desiredWidth = MeasureSpec.makeMeasureSpec(listView.getWidth(), MeasureSpec.EXACTLY);
        for (int i = 0; i < listAdapter.getGroupCount(); i++) {
            View groupItem = listAdapter.getGroupView(i, false, null, listView);
            groupItem.measure(desiredWidth, MeasureSpec.UNSPECIFIED);
            totalHeight += groupItem.getMeasuredHeight();
            for (int j = 0; j < listAdapter.getChildrenCount(i); j++) {
                View listItem = listAdapter.getChildView(i, j, false, null, listView);
                listItem.measure(desiredWidth, MeasureSpec.UNSPECIFIED);

                totalHeight += listItem.getMeasuredHeight();

            }
        }

        LayoutParams params = listView.getLayoutParams();
        int height = totalHeight + (listView.getDividerHeight() * (listAdapter.getGroupCount() - 1));
        if (height < 10)
            height = 200;
        params.height = height;
        listView.setLayoutParams(params);
        listView.requestLayout();

    }

    public static void setExpandableListViewHeightOriginal(ExpandableListView listView, int group) {
        ExpandableListAdapter listAdapter = (ExpandableListAdapter) listView.getExpandableListAdapter();
        int totalHeight = 0;
        int desiredWidth = MeasureSpec.makeMeasureSpec(listView.getWidth(), MeasureSpec.EXACTLY);
        for (int i = 0; i < listAdapter.getGroupCount(); i++) {
            View groupItem = listAdapter.getGroupView(i, false, null, listView);
            groupItem.measure(desiredWidth, MeasureSpec.UNSPECIFIED);

            totalHeight += groupItem.getMeasuredHeight();

            if (((listView.isGroupExpanded(i)) && (i != group)) || ((!listView.isGroupExpanded(i)) && (i == group))) {
                for (int j = 0; j < listAdapter.getChildrenCount(i); j++) {
                    View listItem = listAdapter.getChildView(i, j, false, null, listView);
                    listItem.measure(desiredWidth, MeasureSpec.UNSPECIFIED);

                    totalHeight += listItem.getMeasuredHeight();

                }
            }
        }

        LayoutParams params = listView.getLayoutParams();
        int height = totalHeight + (listView.getDividerHeight() * (listAdapter.getGroupCount() - 1));
        if (height < 10)
            height = 200;
        params.height = height;
        listView.setLayoutParams(params);
        listView.requestLayout();

    }

    public static void showLog(Object mess) {
        try {
            // Log.v("HAI", mess + "");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void showALog(Object mess) {
        try {
            if (BuildConfig.DEBUG)
                Log.e("HAIA", "" + mess);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void showALog(Object mess, Exception e1) {
        try {
            // Log.e("HAIA", "" + mess, e1);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static String formatMoney(Object money) {
        String a = "0";
        try {
            String d = new DecimalFormat("##,##0 đ").format(money);
            a = d.replaceAll(",", ".");
        } catch (Exception e) {
            // TODO: handle exception
        }

        // Locale locale = new Locale("vi_VN");
        // NumberFormat format = NumberFormat.getCurrencyInstance(locale);
        // return format.format(money);
        return a;
    }

    public static String formatNumber(double money) {
        String a = "0";
        try {
            DecimalFormat df = new DecimalFormat("##,##0");
            String d = df.format(money);
            a = d.replaceAll(",", ".");
        } catch (Exception e) {
        }
        // Locale locale = new Locale("vi_VN");
        // NumberFormat format = NumberFormat.getCurrencyInstance(locale);
        // return format.format(money);
        return a;
    }

    public static void pickImage(int code, Activity ac) {
        Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
        getIntent.setType("image/*");
        Intent pickIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        pickIntent.setType("image/*");
        Intent chooserIntent = Intent.createChooser(getIntent, "Select Image");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[]{pickIntent});

        ac.startActivityForResult(chooserIntent, code);
    }


    public static String formatMoney(String money) {
        String a = "0";
        try {
            double money1 = Double.parseDouble(money);
            String d = new DecimalFormat("##,##0đ").format(money1);
            a = d.replaceAll(",", ".");
        } catch (Exception e) {
            // TODO: handle exception
        }

        // Locale locale = new Locale("vi_VN","");
        // NumberFormat format = NumberFormat.getCurrencyInstance(locale);
        // return format.format(money1);
        return a;
    }

    public static String formatMoneyDiem(String money) {
        String a = "0";
        try {
            double money1 = Double.parseDouble(money);
            String d = new DecimalFormat("##,##0 điểm").format(money1);
            a = d.replaceAll(",", ".");
        } catch (Exception e) {
            // TODO: handle exception
        }

        // Locale locale = new Locale("vi_VN","");
        // NumberFormat format = NumberFormat.getCurrencyInstance(locale);
        // return format.format(money1);
        return a;
    }

    public static Currency getCurrency() {
        // Locale l = new Locale("vi_VN", "");
        return Currency.getInstance("vi_VN");

    }

    public static boolean checkInternetConnection(final Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (null == cm) {
            return false;
        }
        if ((cm.getActiveNetworkInfo() != null) && cm.getActiveNetworkInfo().isAvailable() && cm.getActiveNetworkInfo().isConnected()) {
            return true;
        } else {
            return false;
        }
    }


    public static boolean shouldAskPermission() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }


    public static void loadPicasso(Context c, String url, ImageView img) {
        try {
            Picasso.with(c)
                    .load(url)
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)
                    .into(img);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static String getDataString(JSONObject jSonOb, String key) {
        String result = "";
        try {
            if (!jSonOb.getString(key).equals(JSONObject.NULL)) {
                result = jSonOb.getString(key);
            }
        } catch (JSONException e) {
            // showALog("getDataStrign loi");
            // e.printStackTrace();
        }
        return result;
    }

    public static int getDataInt(JSONObject jSonOb, String key) {
        int result = -1;
        try {
            result = jSonOb.getInt(key);
        } catch (JSONException e) {
            // showALog("getDataStrign loi");
            // e.printStackTrace();
        }
        return result;
    }

    public static double getDataDouble(JSONObject jSonOb, String key) {
        double result = -1;
        try {
            result = jSonOb.getDouble(key);
        } catch (JSONException e) {
            // showALog("getDataStrign loi");
            // e.printStackTrace();
        }
        return result;
    }

    public static String check3GorWifi(Context c) {
        ConnectivityManager manager = (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);
        // For 3G check
        boolean is3g = manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).isConnectedOrConnecting();
        // For WiFi Check
        boolean isWifi = manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnectedOrConnecting();
        if (is3g) {
            return "3G";
        } else if (isWifi) {
            return "WIFI";
        } else {
            return "UNKNOW";
        }

    }

    public static long getCurrentTime() {
        long da = 0;
        da = System.currentTimeMillis() / 1000;
        return da;
    }

    public static String md5(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = MessageDigest.getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String getBuildVertion(Activity ac) {
        PackageInfo pInfo;
        String version = "-1";
        try {
            pInfo = ac.getPackageManager().getPackageInfo(ac.getPackageName(), 0);
            version = pInfo.versionName;
        } catch (NameNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return version;
    }


    public static void createEffectTouch(View v, final int inActiveDrawble, final int activeDrawble) {
        v.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN)
                    v.setBackgroundResource(activeDrawble);
                if (event.getAction() == MotionEvent.ACTION_OUTSIDE || event.getAction() == MotionEvent.ACTION_UP)
                    v.setBackgroundResource(inActiveDrawble);
                return false;
            }
        });
    }



    public static void setClickAnimButon(final View v, final int click, final int affterclick) {
        try {
            v.setBackgroundResource(click);
            v.post(new Runnable() {

                @Override
                public void run() {
                    v.setBackgroundColor(affterclick);

                }
            });
        } catch (Exception e) {
            // TODO: handle exception
        }

    }


    public static void showDialog(final String message, final Activity ac) {
        ac.runOnUiThread(new Runnable() {
            public void run() {
                showDialogOk(message, ac).show();
            }
        });
    }

    public static AlertDialog showDialogOk(final String message, final Context ac) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ac);
        builder.setTitle(ac.getString(R.string.app_name));
        builder.setIcon(R.mipmap.ic_launcher);
        builder.setMessage(message).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(final DialogInterface dialog, final int id) {
                // Do nothing.
            }
        });

        AlertDialog alert = builder.create();
        alert.setCancelable(true);
        alert.setCanceledOnTouchOutside(true);
        return alert;
    }


    public static void showToast(final String message, final Context ac) {
        try {
            ((Activity) ac).runOnUiThread(new Runnable() {
                public void run() {
                    // if (HotdealApp.isRunningApp) {
                    showDialogOk(message, ac).show();
                    // }

                    // Toast toast = Toast.makeText(ac, message, duration);
                    // toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    // toast.show();
                }
            });
        } catch (Exception e) {
            // TODO: handle exception
        }

    }

    public static void showToast1(final String message, final int duration, final Context ac) {
        try {
            ((Activity) ac).runOnUiThread(new Runnable() {
                public void run() {
                    // if (HotdealApp.isRunningApp) {
                    Toast toast = Toast.makeText(ac, message, duration);
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    toast.show();
                    // }

                }
            });
        } catch (Exception e) {
            // TODO: handle exception
        }

    }

    public static void showToastThieuThongTin(final Activity ac) {
        ac.runOnUiThread(new Runnable() {
            public void run() {
                Toast toast = Toast.makeText(ac, "Thiếu thông tin", Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                toast.show();
            }
        });
    }

    public static void showToastNoInternet(final Activity ac) {
        ac.runOnUiThread(new Runnable() {
            public void run() {
                Toast toast = Toast.makeText(ac, "Không có kết nối mạng, vui lòng kiểm tra lại kết nối tới internet của bạn", Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                toast.show();
            }
        });
    }


    public static String nameAc;

    public static void setAnimAcTransaction(Context ac) {
        if (ac instanceof Activity) {
            // ((Activity) ac).overridePendingTransition(R.animator.slid_up,
            // R.animator.slid_down);
            ((Activity) ac).overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        }
    }

    public static void startActivityBringToFrond(Context ac, Class<?> class1, String data, String data2) {
        if (checkInternetConnection(ac)) {
            // mTracker = HotdealApp.getDefaultTracker(ac);
            // mTracker.setScreenName(class1.getName());
            // mTracker.send(new HitBuilders.ScreenViewBuilder().build());
            nameAc = class1.getSimpleName();
            Intent intent = new Intent(ac, class1);
            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            intent.putExtra(ConstantValue.DATA_ACTIVITY, data);
            intent.putExtra(ConstantValue.DATA_ACTIVITY2, data2);
            ac.startActivity(intent);
            setAnimAcTransaction(ac);

        } else {
            showToast("Không có kết nối tới Internet", ac);

        }
    }



    public static void startActivity(Context ac, Class<?> class1, String data, String data2) {
        if (checkInternetConnection(ac)) {
            // mTracker = HotdealApp.getDefaultTracker(ac);
            // mTracker.setScreenName(class1.getName());
            // mTracker.send(new HitBuilders.ScreenViewBuilder().build());
            nameAc = class1.getSimpleName();
            Intent intent = new Intent(ac, class1);
//            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra(ConstantValue.DATA_ACTIVITY, data);
            intent.putExtra(ConstantValue.DATA_ACTIVITY2, data2);
            ac.startActivity(intent);
            setAnimAcTransaction(ac);

        } else {
            showToast("Không có kết nối tới Internet", ac);

        }
    }

    public static void startActivityNoCheckIntenet(Context ac, Class<?> class1, String data) {
        nameAc = class1.getSimpleName();
        Intent intent = new Intent(ac, class1);
        intent.putExtra(ConstantValue.DATA_ACTIVITY, data);
        ac.startActivity(intent);
        setAnimAcTransaction(ac);

    }

    /*    public static void startActivityClearStackBringToFrond(Context ac, Class<?> class1, String data) {
            if (null != ac && null != class1) {
                Intent intent = new Intent(ac, class1);
    //            intent.setFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                intent.putExtra(ConstantValue.DATA_ACTIVITY, data);
                ac.startActivity(intent);
                setAnimAcTransaction(ac);
            }

        }*/
    public static void startActivityClearStack(Context ac, Class<?> class1, String data) {
        if (null != ac && null != class1) {
            Intent intent = new Intent(ac, class1);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.putExtra(ConstantValue.DATA_ACTIVITY, data);
            ac.startActivity(intent);
            setAnimAcTransaction(ac);
        }

    }

 /*   public static void startActivityClearStackBringToFrond(Context ac, Class<?> class1, String data) {
        if (null != ac && null != class1) {
            Intent intent = new Intent(ac, class1);
            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.putExtra(ConstantValue.DATA_ACTIVITY, data);
            ac.startActivity(intent);
            setAnimAcTransaction(ac);
        }

    }*/

    public static void startActivityClearStack(Activity ac, Class<?> class1) {
        if (null != ac && null != class1) {
            Intent intent = new Intent(ac, class1);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            ac.startActivity(intent);
            setAnimAcTransaction(ac);
        }
    }

    public static void startActivityForResult(Activity ac, Class<?> class1, int request, String data, String data2) {
        if (null != ac && null != class1) {
            Intent intent = new Intent(ac, class1);
            intent.putExtra(ConstantValue.DATA_ACTIVITY, data);
            intent.putExtra(ConstantValue.DATA_ACTIVITY2, data2);
            ac.startActivityForResult(intent, request);
            setAnimAcTransaction(ac);
        }
    }

    public static void showBigAdmod(Context c) {
    }

    public static String getDataBundle(Activity ac) {
        String value = "";
        Bundle extras = ac.getIntent().getExtras();
        if (extras != null) {
            value = extras.getString(ConstantValue.DATA_ACTIVITY);
        }
        return value;
    }

/*    public static LatLng getCurrentLocation(Context ac) {
        // check if GPS enabled
        GPSTracker gps = new GPSTracker(ac);
        LatLng srcGeoPoint;
        if (gps.canGetLocation()) {
            double latitude = gps.getLatitude();
            double longitude = gps.getLongitude();
            srcGeoPoint = new LatLng(latitude, longitude);
        } else {
            // gps.showSettingsAlert();
            double latitude = -1;
            double longitude = -1;
            srcGeoPoint = new LatLng(latitude, longitude);
        }
        return srcGeoPoint;
    }*/


    public static String getDataBundle2(Activity ac) {
        String value = "";
        Bundle extras = ac.getIntent().getExtras();
        if (extras != null) {
            value = extras.getString(ConstantValue.DATA_ACTIVITY2);
        }
        return value;
    }


    public static String getDataFragment(Fragment f) {
        String data = "";
        try {
            data = f.getArguments().getString(ConstantValue.DATA_FRAGMENT);
        } catch (Exception e) {
            // TODO: handle exception
        }
        return data;
    }

    public static String getDataFragment2(Fragment f) {
        String data = "";
        try {
            data = f.getArguments().getString(ConstantValue.DATA_FRAGMENT2);
        } catch (Exception e) {
            // TODO: handle exception
        }
        return data;
    }


    public static long getLongFromDate(String date, String dateFormat) {
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
        Date oldDate = null;
        try {
            oldDate = formatter.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long oldMillis = (oldDate.getTime()) / 1000;
        return oldMillis;
    }

    public static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public static String getDate(long milliSeconds, String dateFormat) {
        try {
            SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(milliSeconds * 1000);
            return formatter.format(calendar.getTime());
        } catch (Exception e) {
            return "";
        }

    }

    public static String getDate1(long milliSeconds, String dateFormat) {
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
        String dateString = formatter.format(new Date(milliSeconds * 1000));
        return dateString;
    }

    public static String getDateFromDate(Date milliSeconds, String dateFormat) {
        try {
            SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
            String dateString = formatter.format(milliSeconds);
            return dateString;
        } catch (Exception e) {
            return "";
        }

    }

    // public static int dpToPx(int dp) {
    // DisplayMetrics displayMetrics =
    // HotdealApp.context.getResources().getDisplayMetrics();
    // int px = Math.round(dp * (displayMetrics.xdpi /
    // DisplayMetrics.DENSITY_DEFAULT));
    // return px;
    // }
    //
    // public static int pxToDp(int px) {
    // DisplayMetrics displayMetrics =
    // HotdealApp.context.getResources().getDisplayMetrics();
    // int dp = Math.round(px / (displayMetrics.xdpi /
    // DisplayMetrics.DENSITY_DEFAULT));
    // return dp;
    // }


    public static void wakeUpScreen(final Activity ac) {
        try {
            PowerManager pm = (PowerManager) ac.getApplicationContext().getSystemService(Context.POWER_SERVICE);
            WakeLock wakeLock = pm.newWakeLock((PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP), "TAG");
            wakeLock.acquire();
        } catch (Exception e) {
        }

    }

    public static String getKeyHashFacebook(Activity ac) {
        String keyhash = "";
        try {
            PackageInfo info = ac.getPackageManager().getPackageInfo(ac.getApplicationContext().getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                keyhash = Base64.encodeToString(md.digest(), Base64.DEFAULT);

            }
        } catch (NameNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        showALog(keyhash);
        return keyhash;
    }

    public static String getANDROID_ID(final Context ac) {
        return Secure.getString(ac.getContentResolver(), Secure.ANDROID_ID);
    }

    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        } else {
            return capitalize(manufacturer) + " " + model;
        }
    }

    private static String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }

    public static String getDeviceModel() {
        return Build.MODEL;
    }

    public static int getDeviceVersion() {
        return Build.VERSION.SDK_INT;
    }

    public static void hideKeyboard(Activity activity) {
        if (activity != null && activity.getWindow() != null && activity.getWindow().getDecorView() != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(activity.getWindow().getDecorView().getWindowToken(), 0);
        }
    }


}
