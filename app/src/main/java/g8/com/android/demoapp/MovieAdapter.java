package g8.com.android.demoapp;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class MovieAdapter extends BaseAdapter {
    private ArrayList<Model> listData;
    private Activity ac;
    ViewHolder holder;
    NotifyDataAbstract no;

    static class ViewHolder {
        TextView txt;
        TextView txt2;
        ImageView img_cat;
        LinearLayout llImg;
    }

    public MovieAdapter(final Activity _ac, ArrayList<Model> _list, NotifyDataAbstract no1) {
        this.listData = _list;
        this.ac = _ac;
        no = no1;
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Model getItem(int arg0) {
        return listData.get(arg0);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            holder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(ac);
            convertView = inflater.inflate(R.layout.lsv_item_books, null);
            holder.txt = (TextView) convertView.findViewById(R.id.txt_allnews_categty);
            holder.txt2 = (TextView) convertView.findViewById(R.id.txt_allnews_author);
            holder.img_cat = (ImageView) convertView.findViewById(R.id.img_cat);
            holder.llImg = (LinearLayout) convertView.findViewById(R.id.llImg);
            Utilities.setHeight(holder.llImg, 3.5, ac);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final Model md = listData.get(position);
        holder.txt.setText(md.getName());
        holder.txt2.setText(md.getSubName());
        Utilities.loadPicasso(ac, md.getImage(), holder.img_cat);
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                App.setFilmDetail(md);
                Utilities.startActivity(ac, InfoMovie.class, md.getId() + "", md.getName());
            }
        });
        return convertView;
    }
}
