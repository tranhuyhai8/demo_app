package g8.com.android.demoapp.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import java.util.ArrayList;

import g8.com.android.demoapp.App;
import g8.com.android.demoapp.MovieAdapter;
import g8.com.android.demoapp.ConstantValue;
import g8.com.android.demoapp.InfoMovie;
import g8.com.android.demoapp.Model;
import g8.com.android.demoapp.R;
import g8.com.android.demoapp.Utilities;

/**
 * Created by Administrator on 1/16/2017.
 */

public class Fav extends Fragment {
    private GridView gvFav;
    private MovieAdapter adapter;
    private ArrayList<Model> listData;
    private int page = 1;
    private boolean isLoad = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(
                R.layout.fragment_layout, container, false);
        initView(rootView);
        initData();
        return rootView;
    }

    private void initView(View view) {
        gvFav = (GridView) view.findViewById(R.id.gvBook);
    }

    private void initData() {
        if (null == listData)
            listData = new ArrayList<>();
        adapter = new MovieAdapter(getActivity(), listData, null);
        gvFav.setAdapter(adapter);
        gvFav.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                App.setFilmDetail(listData.get(i));
                Utilities.startActivity(getActivity(), InfoMovie.class, listData.get(i).getId() + "", listData.get(i).getName());
            }
        });

    }

    private void loadData() {
        listData.clear();
        listData.addAll(App.getDB().getListFav());
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter intentGetKeySend = new IntentFilter();
        intentGetKeySend.addAction(ConstantValue.REFRESH);
        getActivity().registerReceiver(receiver, intentGetKeySend);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            getActivity().unregisterReceiver(receiver);
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    BroadcastReceiver receiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase(ConstantValue.REFRESH)) {
                loadData();
            }
        }
    };
}
