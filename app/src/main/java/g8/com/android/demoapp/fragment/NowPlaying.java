package g8.com.android.demoapp.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.GridView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import g8.com.android.demoapp.MovieAdapter;
import g8.com.android.demoapp.ConstantValue;
import g8.com.android.demoapp.ParentActivity;
import g8.com.android.demoapp.Model;
import g8.com.android.demoapp.R;
import g8.com.android.demoapp.Utilities;
import g8.com.android.demoapp.connectapi.NotifyDataListener;

/**
 * Created by Administrator on 1/16/2017.
 */

public class NowPlaying extends Fragment {
    private GridView gvBook;
    private MovieAdapter adapter;
    private ArrayList<Model> listData;
    private int page = 1;
    private boolean isLoad = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(
                R.layout.fragment_layout, container, false);
        initView(rootView);
        initData();
        return rootView;
    }

    private void initView(View view) {
        gvBook = (GridView) view.findViewById(R.id.gvBook);
    }

    private void initData() {
        if (null == listData)
            listData = new ArrayList<>();
        adapter = new MovieAdapter(getActivity(), listData, null);
        gvBook.setAdapter(adapter);

        gvBook.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem + visibleItemCount >= totalItemCount) {
                    if (!isLoad) {
                        isLoad = true;
                        Utilities.showALog("EDD");
                        loadData();
                    }
                }
            }

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }
        });
    }

    private void loadData() {
        String url = "https://api.themoviedb.org/3/movie/now_playing?api_key=" + ConstantValue.PRIVATE_KEY + "&language=en-US&page=" + page;
        HashMap<String, String> builder = ((ParentActivity) getActivity()).getDefauldParams(url);
        ((ParentActivity) getActivity()).callServer(getActivity(), builder, true, false, new NotifyDataListener() {
            @Override
            public void onSuccess(JSONObject result) {
                JSONArray array = null;
                try {
                    array = result.getJSONArray("results");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject ob = array.getJSONObject(i);
                        Model md = new Model();
                        md.setImage(ConstantValue.URL_IMAGE + Utilities.getDataString(ob, "poster_path"));
                        md.setDescription(Utilities.getDataString(ob, "overview"));
                        md.setName(Utilities.getDataString(ob, "title"));
                        md.setSubName(Utilities.getDataString(ob, "overview"));
                        md.setId(Utilities.getDataInt(ob, "id"));
                        md.setImgBG(ConstantValue.URL_IMAGE + Utilities.getDataString(ob, "backdrop_path"));
                        listData.add(md);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                page++;
                if (Utilities.getDataInt(result, "total_pages") >= page)
                    isLoad = false;
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(int code, String message) {
            }
        });
    }
}
