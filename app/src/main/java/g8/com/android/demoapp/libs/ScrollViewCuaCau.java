package g8.com.android.demoapp.libs;

import android.content.Context;
import android.util.AttributeSet;
import android.webkit.WebView;

/**
 * Created by Administrator on 11/14/2016.
 */
public class ScrollViewCuaCau extends WebView {
    private onSroll onSroll_;

    public ScrollViewCuaCau(Context context) {
        this(context, null, 0);
    }

    public ScrollViewCuaCau(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ScrollViewCuaCau(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public interface onSroll {
         void onScroll(int y);
    }

    public void setOnScrollY(onSroll mOnEndScrollListener) {
        this.onSroll_ = mOnEndScrollListener;
    }

    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        if (null != onSroll_) {
            onSroll_.onScroll(oldt);
        }
        // Grab the last child placed in the ScrollView, we need it to determinate the bottom position.
//        View view = (View) getChildAt(getChildCount() - 1);
//        HotdealUtilities.showALog(l + "-" + t + " " + oldl + " " + oldt);
//        // Calculate the scrolldiff
//        int diff = (view.getBottom() - (getHeight() + getScrollY()));
//
//        // if diff is zero, then the bottom has been reached
//        if (diff == 0) {
//            // notify that we have reached the bottom
//            Log.d("Scrool", "MyScrollView: Bottom has been reached");
//        }

        super.onScrollChanged(l, t, oldl, oldt);
    }
}
