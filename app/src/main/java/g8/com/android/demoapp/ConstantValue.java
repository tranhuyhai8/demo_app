package g8.com.android.demoapp;

public class ConstantValue {
	public static final String DATA_FRAGMENT = "datafragment";
    public static final String DATA_FRAGMENT2 = "datafragment2";
	public static final String DATA_ACTIVITY = "dataac";
    public static final String DATA_ACTIVITY2 = "dataac2";

	public static final String IS_SUCCESS = "is_success";
    public static final String REFRESH = "gv";

	// param key
    public static final String API = "api";
    public static final String URL_IMAGE = "http://image.tmdb.org/t/p/w500";
    public static final String PRIVATE_KEY = "76571fbf54d155b8532128eccf31c5fc";

}
