package g8.com.android.demoapp;

/**
 * Created by Administrator on 1/16/2017.
 */

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import java.util.ArrayList;

import g8.com.android.demoapp.libs.ExpandableHeightGridView;

public class SlidePageNews extends PagerAdapter {

    private Context mContext;
    private int s;
    ArrayList<Model> listData;
    ArrayList<Model> list;

    public SlidePageNews(Context context, int size, ArrayList<Model> list_) {
        mContext = context;
        s = size;
        listData = list_;
    }

    @Override
    public Object instantiateItem(ViewGroup collection, int position) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.item_similar, collection, false);
        list = new ArrayList<>();
        try {
            int pos = position * 2;
            list.add(listData.get(pos));
            list.add(listData.get((pos + 1)));
        } catch (Exception e) {
            e.printStackTrace();
        }
        MovieAdapter adapter = new MovieAdapter((Activity) mContext, list, null);
        ExpandableHeightGridView gv = (ExpandableHeightGridView) layout.findViewById(R.id.gv);
        gv.setExpanded(true);
        gv.setAdapter(adapter);
        collection.addView(layout);
        return layout;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return s;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }


}
