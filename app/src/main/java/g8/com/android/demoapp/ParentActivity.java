package g8.com.android.demoapp;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import g8.com.android.demoapp.connectapi.NotifyDataListener;
import g8.com.android.demoapp.connectapi.VolleyRequestCustom;

/**
 * Created by Administrator on 12/12/2016.
 */

public class ParentActivity extends AppCompatActivity {
    private Dialog mProgress;
    private String message;
    private VolleyRequestCustom jsonObjRequest;
    private String errorMess = "Lỗi trong quá trình tải dữ liệu, Vui lòng thử lại";
    private RequestQueue mVolleyQueue;

    public void showProgress(Activity activity) {
        if (!isShowPro) {
            try {
                mProgress = new Dialog(activity);
                mProgress.getWindow();
                mProgress.requestWindowFeature(Window.FEATURE_NO_TITLE);
                mProgress.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                mProgress.setContentView(R.layout.loading);
                mProgress.setCancelable(false);
                mProgress.setCanceledOnTouchOutside(false);
                mProgress.show();
                isShowPro = true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void stopProgress() {
        try {
            mProgress.dismiss();
            isShowPro = false;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private boolean isShowPro = false;

    public void callServer(final Activity activity, final HashMap<String, String> parameters, final boolean showPro, final boolean isPost, final NotifyDataListener notifyDataListener) {
        Utilities.showALog(parameters.toString());
        if (showPro) {
            showProgress(activity);

        }
        int method;
        String url;
        if (isPost) {
            method = Request.Method.POST;
//            url = ConstantValue.URL_SERVER;
        } else {
            method = Request.Method.GET;
//            url = ConstantValue.URL_SERVER + "/" + parameters.get(ConstantValue.API);
        }
        url =parameters.remove(ConstantValue.API);
        Utilities.showALog(url);
        jsonObjRequest = new VolleyRequestCustom(method, url, parameters, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Utilities.showALog("response: " + response.toString());
                try {
//                    jsonObjectInterface.callResultJOb(activity, response);
                    notifyDataListener.onSuccess(response);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (showPro) {
                    stopProgress();
                }


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                if (error instanceof NetworkError) {
                    setMessage(errorMess + " (Network Error)");
                } else if (error instanceof ServerError) {
                    setMessage(errorMess + " (Server Error)");
                } else if (error instanceof AuthFailureError) {
                    setMessage(errorMess + " (AuthFailure Error)");
                } else if (error instanceof ParseError) {
                    setMessage(errorMess + " (Parse Error)");
                } else if (error instanceof NoConnectionError) {
                    setMessage(errorMess + " (No Connection Error)");
                } else if (error instanceof TimeoutError) {
                    setMessage(errorMess + " (Timeout Error)");
                }
                JSONObject son = null;
                try {
                    son = new JSONObject(error.getMessage());
                    setMessage(son.getString("message").toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                notifyDataListener.onFailure(0, getMessage());
//                jsonObjectInterface.callResultJOb(activity, son);
                if (showPro) {
                    stopProgress();
                }
            }
        });

        jsonObjRequest.setRetryPolicy(new DefaultRetryPolicy(20000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        jsonObjRequest.setTag("fuck");
        getVolleyQueue(activity).add(jsonObjRequest);
    }

    private RequestQueue getVolleyQueue(Activity ac) {
        if (null == mVolleyQueue) {
            return mVolleyQueue = Volley.newRequestQueue(ac);
        }
        return mVolleyQueue;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public HashMap<String, String> getDefauldParams(String API) {
        HashMap<String, String> parameters = new HashMap<String, String>();
        parameters.put(ConstantValue.API, API);
        return parameters;
    }
}
