package g8.com.android.demoapp;

import android.app.Application;
import android.content.Context;

import g8.com.android.demoapp.libs.DatabaseHandler;

/**
 * Created by Administrator on 5/11/2016.
 */
public class App extends Application {
    private static int device_width = 0;
    private static int device_height = 0;
    private static Context context;
    private static DatabaseHandler db;
    private static Model filmDetail;


    public static Model getFilmDetail() {
        return filmDetail;
    }

    public static void setFilmDetail(Model filmDetail) {
        App.filmDetail = filmDetail;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
    }

    public static int getDevice_height() {
        return device_height;
    }

    public static void setDevice_height(int device_height) {
        App.device_height = device_height;
    }

    public static int getDevice_width() {
        return device_width;
    }

    public static void setDevice_width(int device_width) {
        App.device_width = device_width;
    }

    public static Context getContext() {
        return context;
    }


    synchronized public static DatabaseHandler getDB() {
        if (null == db) {
            db = new DatabaseHandler(context);
        }
        return db;
    }


}
