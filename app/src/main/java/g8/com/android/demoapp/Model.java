package g8.com.android.demoapp;

import java.io.Serializable;

/**
 * Created by Administrator on 1/4/2017.
 */

public class Model implements Serializable {
    private String name;
    private String subName;
    private String image;

    private int id;
    private String imgBG;
    private int view;
    private String description;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getSubName() {
        return subName;
    }

    public void setSubName(String subName) {
        this.subName = subName;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getImgBG() {
        return imgBG;
    }

    public void setImgBG(String imgBG) {
        this.imgBG = imgBG;
    }

    public int getView() {
        return view;
    }

    public void setView(int view) {
        this.view = view;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}

