package g8.com.android.demoapp.connectapi;

import org.json.JSONObject;

public interface NotifyDataListener {
	
	public static final int NOTIFY_OK = 0;
	public static final int NOTIFY_FAILED = 1;
	
	public void onSuccess(JSONObject result);
    public void onFailure(int code, String message);

}
